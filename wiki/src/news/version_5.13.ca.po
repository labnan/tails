# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-05-15 17:21+0200\n"
"PO-Revision-Date: 2023-12-05 23:44+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 5.13\"]]\n"
msgstr "[[!meta title=\"Tails 5.13\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue, 16 May 2023 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 16 May 2023 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"features\">New features</h1>\n"
msgstr "<h1 id=\"features\">Noves funcionalitats</h1>\n"

#. type: Bullet: '- '
msgid ""
"Add `curl`, a command line tool to download over HTTPS, FTP, and other "
"protocols."
msgstr ""
"S'ha afegit `curl`, una eina de línia d'ordres per baixar mitjançant HTTPS, "
"FTP i altres protocols."

#. type: Plain text
#, no-wrap
msgid "  `curl` can be useful for online investigations as an alternative to `wget`.\n"
msgstr ""
"  `curl` pot ser útil per a investigacions en línia com a alternativa a "
"`wget`.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  Everything you do with `curl` goes through the Tor network. If you want to\n"
"  use `curl` on a local network, use `/usr/bin/curl` instead.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Canvis i actualitzacions</h1>\n"

#. type: Bullet: '- '
msgid ""
"Use LUKS2 by default for all new Persistent Storage and LUKS encrypted "
"volumes. LUKS2 provide stronger cryptography by default."
msgstr ""
"S'utilitza LUKS2 de manera predeterminada per a tots els nous volums "
"d'Emmagatzematge Persistent i encriptats amb LUKS. LUKS2 ofereix una "
"criptografia més forta per defecte."

#. type: Plain text
#, no-wrap
msgid ""
"  We will provide a migration plan from LUKS1 to LUKS2 for existing Persistent\n"
"  Storage in Tails 5.14 (early June).\n"
msgstr ""
"  Proporcionarem un pla de migració de LUKS1 a LUKS2 per als "
"Emmagatzematges\n"
"  Persistents existents a Tails 5.14 (principis de juny).\n"

#. type: Plain text
msgid ""
"- Update *Tor Browser* to [12.0.6](https://blog.torproject.org/new-release-"
"tor-browser-1206)."
msgstr ""
"- S'ha actualitzat el *Navegador Tor* a la versió [12.0.6](https://blog."
"torproject.org/new-release-tor-browser-1206)."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Problemes solucionats</h1>\n"

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog desc=\"changelog"
"\"]]."
msgstr ""
"Per a més detalls, llegiu el nostre [[!tails_gitweb debian/changelog desc="
"\"llistat de canvis\"]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Problemes coneguts</h1>\n"

#. type: Plain text
msgid "None specific to this release."
msgstr "Cap específic d'aquesta versió."

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr "Vegeu la llista de [[problemes de llarga durada|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.13</h1>\n"
msgstr "<h1 id=\"get\">Obtenir Tails 5.13</h1>\n"

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"Per actualitzar el vostre llapis USB de Tails i mantenir el vostre "
"Emmagatzematge Persistent"

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.13."
msgstr ""
"- Les actualitzacions automàtiques estan disponibles des de Tails 5.0 o "
"posterior a la versió 5.13."

#. type: Plain text
#, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Podeu [[reduir la mida de la baixada|doc/upgrade#reduce]] de futures\n"
"  actualitzacions automàtiques fent una actualització manual a la darrera "
"versió.\n"

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Si no podeu fer una actualització automàtica o si Tails no s'inicia després "
"d'una actualització automàtica, proveu de fer una [[actualització manual|doc/"
"upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Per instal·lar Tails en un nou llapis USB"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Seguiu les nostres instruccions d'instal·lació:"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Instal·lar des de Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Instal·lar des de macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Instal·lar des de Linux|install/linux]]"

#. type: Bullet: '  - '
msgid ""
"[[Install from Debian or Ubuntu using the command line and GnuPG|install/"
"expert]]"
msgstr ""
"[[Instal·lar des de Debian o Ubuntu mitjançant la línia d'ordres i GnuPG|"
"install/expert]]"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>L'Emmagatzematge Persistent del llapis USB es "
"perdrà si\n"
"instal·leu en comptes d'actualitzar.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Per només baixar"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.13 directly:"
msgstr ""
"Si no necessiteu instruccions d'instal·lació o actualització, podeu baixar "
"Tails 5.13 directament:"

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Per a llapis USB (imatge USB)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Per a DVD i màquines virtuals (imatge ISO)|install/download-iso]]"
