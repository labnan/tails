# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: italian\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-12-01 03:08+0000\n"
"PO-Revision-Date: 2023-11-21 18:16+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: ita <transitails@inventati.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Content of: <div>
msgid "[[!meta title=\"Install Tails\"]]"
msgstr "[[!meta title=\"Installa Tails\"]]"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]] "
#| "[[!meta stylesheet=\"install\" rel=\"stylesheet\" title=\"\"]]"
msgid "[[!meta stylesheet=\"install\" rel=\"stylesheet\" title=\"\"]]"
msgstr ""
"[[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"install\" rel=\"stylesheet\" title=\"\"]]"

#. type: Content of: <div><div><div>
msgid "[[ [[!img install/inc/icons/windows.png link=\"no\" alt=\"\"]]"
msgstr "[[ [[!img install/inc/icons/windows.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><div><p>
msgid "Windows"
msgstr "Windows"

#. type: Content of: <div><div><div>
msgid "|install/windows]]"
msgstr "|install/windows]]"

#. type: Content of: <div><div><div>
msgid "[[ [[!img install/inc/icons/apple.png link=\"no\" alt=\"\"]]"
msgstr "[[ [[!img install/inc/icons/apple.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><div><p>
msgid "macOS"
msgstr "macOS"

#. type: Content of: <div><div><div>
msgid "|install/mac]]"
msgstr "|install/mac]]"

#. type: Content of: <div><div><div>
msgid "[[ [[!img install/inc/icons/linux.png link=\"no\" alt=\"\"]]"
msgstr "[[ [[!img install/inc/icons/linux.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><div><p>
msgid "Linux"
msgstr "Linux"

#. type: Content of: <div><div><div>
msgid "|install/linux]]"
msgstr "|install/linux]]"

#. type: Content of: <div><div><div>
msgid "[[ [[!img install/inc/icons/expert.png link=\"no\" alt=\"\"]]"
msgstr "[[ [[!img install/inc/icons/expert.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><div><p>
msgid "Terminal"
msgstr "Terminal"

#. type: Content of: <div><div><div><p>
msgid "Debian or Ubuntu using the command line and GnuPG"
msgstr "Debian o Ubuntu usando la linea di comando e GnuPG"

#. type: Content of: <div><div><div>
msgid "|install/expert]]"
msgstr "|install/expert]]"

#. type: Content of: <div><p>
msgid ""
"If you know someone you trust who uses Tails already, you can install your "
"Tails by cloning their Tails:"
msgstr ""
"Se conosci qualcuno di cui ti fidi che già usa Tails, puoi installare il tuo "
"Tails clonando il suo:"

#. type: Content of: <div><ul><li>
msgid "[[Install by cloning from another Tails on PC|install/clone/pc]]"
msgstr ""
"[[Installa tramite clonazione da un altro Tails sul PC|install/clone/pc]]"

#. type: Content of: <div><ul><li>
msgid "[[Install by cloning from another Tails on Mac|install/clone/mac]]"
msgstr ""
"[[Installa tramite clonazione da un altro Tails su Mac|install/clone/mac]]"

#. type: Content of: <h3>
msgid "Other options:"
msgstr "Altre opzioni:"

#. type: Content of: <ul><li>
msgid "[[Download only (for USB sticks)|install/download]]"
msgstr "[[Download solo (per chiavette USB)|install/download]]"

#. type: Content of: <ul><li>
msgid "[[Burn a Tails DVD|install/dvd]]"
msgstr "[[Masterizzare un DVD Tails|install/dvd]]"

#. type: Content of: <ul><li>
msgid "[[Run Tails in a virtual machine|install/vm]]"
msgstr "[[Eseguire Tails in una macchina virtuale|install/vm]]"

#. type: Content of: <div><p>
msgid ""
"<strong>Tails doesn't work on smartphones or tablets.</strong> The hardware "
"of smartphones and tablets is very different from the hardware of "
"computers.  For now, it's impossible to make smartphone and tablet hardware "
"work with Linux distributions like Tails."
msgstr ""
"<strong>Tails non funziona su smartphone o tablet.</strong> L'hardware di "
"smartphone e tablet è molto diverso da quello dei computer.  Per ora, è "
"impossibile far funzionare l'hardware di smartphone e tablet con "
"distribuzioni Linux come Tails."

#~ msgid "Thank you for your interest in Tails."
#~ msgstr "Grazie del tuo interesse per Tails."

#~ msgid ""
#~ "Installing Tails can be quite long but we hope you will still have a good "
#~ "time :)"
#~ msgstr ""
#~ "Installare Tails può richiedere parecchio tempo ma noi speriamo che ti "
#~ "diverta comunque :)"

#~ msgid ""
#~ "We will first ask you a few questions to choose your installation "
#~ "scenario and then guide you step by step."
#~ msgstr ""
#~ "Per prima cosa ti faremo alcune domande per scegliere il tuo scenario "
#~ "d'installazione e poi ti guideremo passo per passo."

#~ msgid "Which operating system are you installing Tails from?"
#~ msgstr "Da quale sistema operativo stai installando Tails?"

#~ msgid "[["
#~ msgstr "[["

#~ msgid "Download only:"
#~ msgstr "Scaricare soltanto:"

#~ msgid "[[For DVDs (ISO image)|install/dvd-download]]"
#~ msgstr "[[Per DVD (immagine ISO)|install/dvd-download]]"

#~ msgid "Debian, Ubuntu, or Mint"
#~ msgstr "Debian, Ubuntu o Mint"

#~ msgid "|install/debian]]"
#~ msgstr "|install/debian]]"

#~ msgid "<small>(Red Hat, Fedora, etc.)</small>"
#~ msgstr "<small>(Red Hat, Fedora, etc.)</small>"

#~ msgid "Let's start the journey!"
#~ msgstr "Iniziamo!"

#~ msgid "Welcome to the"
#~ msgstr "Benvenuta nella"

#~ msgid "<strong>Tails Installation Assistant</strong>"
#~ msgstr "<strong>Installazione assistita di Tails</strong>"

#~ msgid ""
#~ "The following set of instructions is quite new. If you face problems "
#~ "following them:"
#~ msgstr ""
#~ "Il seguente set di istruzioni è relativamente nuovo, se incontri un "
#~ "problema seguendolo, per piacere:"

#~ msgid "[[Report your problem.|support/talk]]"
#~ msgstr "[[Riferisci il problema.|support/talk]]"

#~ msgid ""
#~ "Try following our old instructions for [[downloading|/download]] or "
#~ "[[installing|doc/first_steps]] instead."
#~ msgstr ""
#~ "Prova altrimenti a seguire le vecchie istruzioni per [[scaricare|/"
#~ "download]] o [[installare|doc/first_steps]]."
