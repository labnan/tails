# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-01-30 12:41+0000\n"
"PO-Revision-Date: 2023-11-03 18:12+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Using the screen keyboard\"]]\n"
msgstr "[[!meta title=\"Ús del teclat de pantalla\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"If you think that the computer that you are using is not trustworthy,\n"
"for example when using a public computer in a library, everything that\n"
"you type might be recorded by a hardware [[!wikipedia Keystroke_logging\n"
"desc=\"keylogger\"]].\n"
msgstr ""
"Si creieu que l'ordinador que utilitzeu no és fiable,\n"
"per exemple quan s'utilitza un ordinador públic en una biblioteca, tot\n"
"el que escriviu pot ser registrat per un [[!wikipedia Keystroke_logging\n"
"desc=\"enregistrador de tecles\"]].\n"

#. type: Plain text
#, no-wrap
msgid ""
"You can use the [<span\n"
"class=\"application\">GNOME screen keyboard</span>](https://help.gnome.org/users/gnome-help/stable/keyboard-osk.html.en)\n"
"to protect you against a hardware keylogger when typing\n"
"passwords and sensitive text. To display the screen keyboard,\n"
"[[see the corresponding documentation|first_steps/accessibility]].\n"
msgstr ""
"Podeu utilitzar el [[<span\n"
"class=\"application\">Teclat de pantalla de GNOME</span>](https://help.gnome."
"org/users/gnome-help/stable/keyboard-osk.html.ca)\n"
"per protegir-vos contra un maquinari enregistrador de tecles quan escriviu\n"
"contrasenyes i text sensible. Per mostrar el teclat de pantalla,\n"
"[[vegeu la documentació corresponent|first_steps/accessibility]].\n"
